#!/bin/bash

FILE=$1


compatible=`grep "compatible\s*:" $FILE | grep "\".*\"" -o | grep "[^\"]*" -o`

licens=`grep $compatible drivers -lr | xargs head -n 1 | grep "GPL.*" -o`

path=`echo $FILE | cut -b 37- | sed 's/.txt/.yaml/g'`

title=`head -n 1 $FILE`

maintainer1=`./scripts/get_maintainer.pl $FILE | grep -oE "^[^>]+>" | head -n 1`

maintainer2=`./scripts/get_maintainer.pl $FILE | grep -oE "^[^>]+>" | head -n 2 | tail -n 1`

description=`sed '/required properties:/Iq' $FILE | sed '$ d' | sed 's/^/  /g' `

required=`sed -n '/^required properties:$/I,//p' $FILE | grep "^-" | grep -oE "^[^:]+" | cut -c2-`

example=`sed -n '/^examples\?:$/I,//p' $FILE | grep -vi example | sed 's/\t/        /g'`

echo "# SPDX-License-Identifier: $licens
%YAML 1.2
---
\$id: http://devicetree.org/schemas/${path}#
\$schema: http://devicetree.org/meta-schemas/core.yaml#

title: $title

maintainers:
  - $maintainer1
  - $maintainer2

description: |
  $description

properties:
  compatible:
  const: $compatible

required:
  - compatible
  - $required

additionalProperties: false

examples:
  - |
    $example
"
